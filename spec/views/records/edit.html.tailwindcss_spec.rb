require 'rails_helper'

RSpec.describe "records/edit", type: :view do
  let(:record) {
    Record.create!(
      body: "MyText"
    )
  }

  before(:each) do
    assign(:record, record)
  end

  it "renders the edit record form" do
    render

    assert_select "form[action=?][method=?]", record_path(record), "post" do

      assert_select "textarea[name=?]", "record[body]"
    end
  end
end
