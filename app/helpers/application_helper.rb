# frozen_string_literal: true

# ApplicationHelper is a module that contains helper methods
# that are available to all controllers and views in the application.
module ApplicationHelper
end
