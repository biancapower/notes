# frozen_string_literal: true

# ApplicationRecord is a class that contains methods that are
# available to all models in the application.
class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
end
