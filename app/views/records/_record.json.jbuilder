json.extract! record, :id, :body, :created_at, :updated_at
json.url record_url(record, format: :json)
