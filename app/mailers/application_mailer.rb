# frozen_string_literal: true

# ApplicationMailer is a class that contains methods that are
# available to all mailers in the application.
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
